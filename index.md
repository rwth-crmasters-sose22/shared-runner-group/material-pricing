---
title: Interactive Cost Analysis
description: Interactive cost estimations 
author: Htoo Inzali, Ye Lu, Rebeca García Julio
keywords: marp,marp-cli,slide
url: https://rwth-crmasters-sose22.gitlab.io/shared-runner-group/material-pricing/
marp: true
image: Please replace this with a URL of an avatar/icon of your presentation
footer: DDP Concept Presentation

---

# Interactive Cost Analysis
## A Revit Addin

## Rebeca, Georgy & Su 

---

<style>
img[alt~="center"], .center, .fence {
  display: block;
  margin: 0 auto;
}
kbd {
    background-color: #eee;
    border-radius: 3px;
    border: 1px solid #b4b4b4;
    box-shadow: 0 1px 1px rgba(0, 0, 0, .2), 0 2px 0 0 rgba(255, 255, 255, .7) inset;
    color: #333;
    display: inline-block;
    font-size: .85em;
    font-weight: 700;
    line-height: 1;
    padding: 2px 4px;
    white-space: nowrap;
}
.float-right {
    float: right;
}
.fence img {
    width: auto;
    max-width: 100%;
    height: auto;
    max-height: 100%;
}
.margin-top {
    margin-top: 1em;
}
.padding-top {
    padding-top: 1em;
}
</style>

# Collected Stories 
- The designer doesn't have accountable cost information to estimate the project budget.
- _"Light decisions"_ about additional or extra works due to ignorance of the incidence of these in the budget.
- They give the design drawings or model-based quantities to the quantity surveyor for cost estimation. And they calculate the cost and propose to the client. It is **a one-way communication**. 
- If there is a change needed to be made, the process goes again. It causes **rework and delays**.
- **39% of projects fail due to the lack of proper planning**

---
_“We used to do all of our material and subcontract pricing requests via email and spreadsheets. While the process worked, it was time-consuming and cumbersome to manage"_

==**PROJECTS ON TIME AND WITHIN THE BUDGET  DEPENDS ON EARLY STAGES OF THE DESIGN**==

---
# Expert Opinion/ Industrial Feedback

Collecting expert opinion and industrial feedback is still in progress. 

According to the informal/verbal feedback, most of the revit users expecially designers/architects seem to be interested. 
Some have highlighed about the similar software and BIM workflow (which most of them require more than one software other than revit). 
Detailed presentation of the concept and propsed addin features are yet to be presented to them. 

---


# Concept Development (Mindmap)


::: fence

@startuml

@startmindmap

*[#Orange] Cost Planning and Control
** Quantity Takeoff
*** Traditional Quantity Takeoff (Non.model based)
*** BIM-based Quantity Takeoff
**[#Orange] Cost Estimating
***[#Orange] Estimating Softwares
**** Detailed Estimation
***** Excel sheets
***** ...
****[#Orange] Visual Data Analysis 
***** Non-Interactive
*****[#Orange] Interactive
****** Software
****** Cloud-based interface
****** Automation-enable
******[#Orange] Add-in
*******[#Orange] Options to link to external cost data base 
*******[#Orange] Export the result to other format (eg. power BI, PDF,...) 
*******[#Orange] Cost comparison
*******[#Orange] Real-time changes
*******[#Orange] Connected Workflow
*******[#Orange] Advanced analysis and optimization

@endmindmap

@enduml

:::


---

> Most of the BIM processes and workflows are clear and established for the geometric and visualization tools of the technology but not for **data management**.

The main objective of our concept is to dig into better practices to turn raw Revit data into great visualizations that ==_will allow better decisions and workflows on construction projects._==

---
# Why? 

Traditionally, the material quantities are taken off from BIM model. Export to required format (CAD, PDF, Excel, etc.). Then, import into the estimating software or excel sheets with cost data to estimate the cost. 
(or)
The material price will be manually key into the revit schedule and calculate. 

> **Boring detailed cost estimates** 
**Repetative export and import**
**Requirement of additional software and script**

---
# How? 

- Types of material and quantities will be extracted from Revit.
- The detailed cost data will be synced from orginisational cost library or external cost references. 
- Propsed addin database will perform all the calculation, analyse the results and generate the visual data. 

![w:800 h:500 bg right:55%](https://gitlab.com/rwth-crmasters-sose22/shared-runner-group/material-pricing/-/raw/main/images/how-to-plan.jpeg)


---
# How?
## Proposed Addin Features

- Track and compare the selected design changes ("Design Option" in Revit)
- Record the result (options to export and save in the revit file itself)
- Option to choose estimation methods and other cost components (contigencies and budget adjustment options)
- Option to sort the analysis result by area/room/level.

##### _Notes:_

> _***Real time analysis would be most likely for the current working design.
***Comparison is for selected design options._

---


## Reference Project - [Tally](https://www.youtube.com/watch?v=2t64Tsa_4RQ)
![w:950 h:550 center](https://gitlab.com/rwth-crmasters-sose22/shared-runner-group/material-pricing/-/raw/main/images/sample3.png)



---

# Open Sources 

* [Apache/Echarts](https://github.com/apache/echarts) powerful charting and visualization library offering an easy way of adding intuitive, interactive, and highly customizable charts to commercial products
* [ToastUI Chart](https://github.com/nhn/tui.chart) makes data pop and presents it in a manner that is easy to understand. Furthermore, it provides a wide range of theme options for customizing the charts to be suitable for all services.
* [Awsome BIM](https://github.com/mitevpi/awesome-bim) A curated collection of useful BIM (Building Information Modeling) resources, libraries, software and frameworks for AEC research, application development, data analysis, and general computation

---
* [Especle|Sharp](https://github.com/specklesystems/speckle-sharp) 
  - Interoperability: get your CAD and BIM models into other software without exporting or importing
  - Real time: get real time updates and notifications and changes
  - Speckle connectors are plugins for the most common software used in the industry such as Revit, Rhino, Grasshopper, AutoCAD, Civil 3D, Excel, Unreal Engine, Unity, QGIS, Blender and more!

---



